;;; hdic-json.el --- Utility to dump the HDIC database as JSON-LD ; -*- coding: utf-8-mcs-er; -*-

;; Copyright (C) 2023 MORIOKA Tomohiko

;; Author: MORIOKA Tomohiko <tomo.git@chise.org>
;; Keywords: HDIC, CHISE

;; This file is a part of HDIC-JSON.

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 2, or (at
;; your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Code:

(require 'cwiki-common)
(require 'concord-kanbun-dic)

(defvar hdic-json-dump-directory "/usr/local/share/hdic/data/")
(defvar hdic-json-est-url-base "https://www.chise.org/est/view")
(defvar hdic-json-pron-object-base "https://www.chise.org/est/view/hanzi-syllable/rep.id=")

(defun hdic-syp-decode-char (syp-id)
  (map-char-attribute
   (lambda (c v)
     (when (eq v syp-id)
       c))
   '=hdic-syp-entry-id))

(defun hdic-json-format-props (props)
  (let ((dest "")
	key val)
    (while props
      (setq key (pop props)
	    val (pop props))
      (if (symbolp key)
	  (setq key (symbol-name key)))
      (if (eq (aref key 0) ?:)
	  (setq key (substring key 1)))
      (setq dest
	    (format "%s, \"%s\": \"%s\""
		    dest key
		    (format "%s" val)
		    )))
    dest))

(defun hdic-json-object-iri (object)
  (format "%s/%s/%s"
	  hdic-json-est-url-base
	  (est-object-genre object)
	  (www-uri-encode-object object)))

(defun hdic-json-feature-name-iri (feature-name)
  (format "%s/feature/%s"
	  hdic-json-est-url-base
	  (www-uri-encode-feature-name feature-name)))
  
(defun hdic-json-format-pron (props children iri-base number)
  (let (subtype)
    (setq subtype (or (plist-get props 'type)
		      'fanqie))
    (format
     "{ \"@type\": \"Pron\", \"@id\": \"%s#s%d\", \"description\": [ %s ] }"
     iri-base number
     (mapconcat
      (lambda (cell)
	(cond
	 ((concord-object-p cell)
	  (cond
	   ((eq subtype 'fanqie)
	    (format "{ \"@type\": \"PronFanqie\", \"@id\": \"%s%s\", \"name\": \"%s\" }"
		    hdic-json-pron-object-base
		    (concord-object-id cell)
		    (concord-object-get cell '=fanqie))
	    )
	   (t
	    (format "{ \"@id\": \"%s\", \"@type\": \"hdic:%s\" }"
		    (concord-object-id cell)
		    (concord-object-genre cell))
	    ))
	  )
	 ((consp cell)
	  (cond
	   ((memq (car cell) '(prefix exact suffix))
	    (format "{ \"@type\": \"Pron%s\", \"name\": %S }"
		    (capitalize (symbol-name (car cell))) (nth 2 cell))
	    )
	   (t
	    (format "{ \"@type\": \"hdic:%s\", \"name\": %S }"
		    (car cell) (nth 2 cell))
	    )))))
      children ", "))))

(defun hdic-json-format-cite (props children iri-base number)
  (let ((iri (format "%s#s%d" iri-base number))
	obj name bib-name-main)
    (format
     "{ \"@type\": \"Quotation\", \"@id\": \"%s\", \"description\": [ %s ] }"
     iri
     (mapconcat
      (lambda (cell)
	(cond
	 ((concord-object-p cell)
	  (format "{ \"@id\": \"%s\", \"@type\": \"hdic:%s\" }"
		  (concord-object-id cell)
		  (concord-object-genre cell))
	  )
	 ((consp cell)
	  (cond
	   ((eq (car cell) 'object)
	    (cond
	     ((eq (plist-get (nth 1 cell) :tag) 'bibl)
	      (setq obj (plist-get (nth 1 cell) :object))
	      (format "{ \"@type\": \"Bibl\", \"@id\": \"%s/bibl\", %s, \"name\": %S }"
		      iri
		      (cond
		       ((concord-object-p obj)
			(cond ((eq (concord-object-genre obj) 'bibliography)
			       (format "\"isPartOf\": \"%s\""
				       (hdic-json-object-iri obj))
			       )
			      ((eq (concord-object-genre obj) 'creator)
			       (format "\"creator\": { \"@id\": \"%s\", \"name\": \"%s\" }"
				       (hdic-json-object-iri obj)
				       (nth 2 cell))
			       )
			      (t
			       (format "\"sameAs\": \"%s\""
				       (hdic-json-object-iri obj))
			       ))
			)
		       ((characterp obj)
			(if (encode-char obj '=shuowen-jiguge)
			    (format "\"isPartOf\": \"%s/bibliography/rep.chise-bib-id=shuowen\", \"sameAs\": \"%s\""
				    hdic-json-est-url-base
				    (hdic-json-object-iri obj))
			  (format "\"sameAs\": \"%s\""
				  (hdic-json-object-iri obj)))
			)
		       (t
			(format "\"sameAs\": \"%s\""
				(hdic-json-object-iri obj))
			))
		      (nth 2 cell))
	      )
	     (t
	      (format "{ \"@type\": \"hdic:object\"%s, \"description\": %s }"
		      (if props
			  (hdic-json-format-props props)
			"")
		      (hdic-json-format-list children iri))
	      ))
	    )
	   ((memq (car cell) '(prefix exact suffix))
	    (format "{ \"@type\": \"Pron%s\", \"name\": %S }"
		    (capitalize (symbol-name (car cell))) (nth 2 cell))
	    )
	   ((eq (car cell) 'bibl)
	    (setq name (nth 2 cell))
	    (setq bib-name-main
		  (if (string-match "《\\(.+\\)》" name)
		      (match-string 1 name)
		    name))
	    (setq obj (or (concord-decode-object '=title bib-name-main 'bibliography)
			  (concord-decode-object '=abbreviated-title bib-name-main 'bibliography)
			  (concord-decode-object '=abbreviated-title@_1 bib-name-main 'bibliography)
			  (concord-decode-object '=abbreviated-title@_2 bib-name-main 'bibliography)))
	    (if obj
		(format
		 "{ \"@type\": \"Bibl\", \"isPartOf\": \"%s\", \"name\": \"%s\" }"
		 (hdic-json-object-iri obj)
		 (nth 2 cell))
	      (format "{ \"@type\": \"Bibl\", \"name\": %S }"
		      (nth 2 cell)))
	    )
	   (t
	    (format "{ \"@type\": \"%s\", \"name\": %S }"
		    (capitalize (symbol-name (car cell))) (nth 2 cell))
	    ))
	  )
	 ((stringp cell)
	  (format "{ \"name\": %S }" cell)
	  )))
      children ", "))))

(defun hdic-json-format-relation (props children iri-base number)
  (let ((iri (format "%s#s%d" iri-base number))
	(feature (plist-get props :feature)))
    (format
     "{ \"@type\": \"%s\", \"@id\": \"%s\", %s }"
     (cond ((eq (plist-get props :type) 'synonym)
	    'SynChar)
	   (t
	    (capitalize (symbol-name (plist-get props :type)))
	    ))
     iri
     (mapconcat
      (lambda (cell)
	(cond
	 ((consp cell)
	  (cond
	   ((eq (car cell) 'predicate)
	    (format "\"relation\": \"%s/predicate\", \"prefix\": { \"@type\": \"chise:Feature\", \"@id\": \"%s/predicate\", \"sameAs\": \"%s\", \"chise:featureName\": \"%s\", \"name\": %s }"
		    iri iri
		    (hdic-json-feature-name-iri (plist-get (nth 1 cell) :feature))
		    (plist-get (nth 1 cell) :feature)
		    (cond
		     ((stringp (nth 2 cell))
		      (format "\"%s\"" (nth 2 cell))
		      )
		     ((consp (nth 2 cell))
		      (format "\"%s\"" (nth 2 (nth 2 cell)))
		      )
		     (t
		      (format "%S" (nth 2 cell))
		      )))
	    )
	   ((eq (car cell) 'object)
	    (if feature
		(format
		 "\"target\": { \"@type\": \"VariantDesc\", \"@id\": \"%s/target\", \"sameAs\": \"%s\", \"chise:featureName\": \"%s\", \"name\": %S }"
		 iri
		 (hdic-json-object-iri
		  (plist-get (nth 1 cell) :object))
		 feature
		 (nth 2 cell))
	      (format "\"target\": { \"@type\": \"chise:Character\", \"@id\": \"%s\", \"name\": %S }"
		      (hdic-json-object-iri
		       (plist-get (nth 1 cell) :object))
		      (nth 2 cell)))
	    )
	   ((eq (car cell) 'target)
	    (format "\"target\": { \"@type\": \"chise:Character\", \"@id\": \"%s\", \"name\": \"%c\" }"
		    (hdic-json-object-iri
		     (nth 2 cell))
		    (nth 2 cell))
	    )
	   ((memq (car cell) '(prefix exact suffix))
	    (format "\"%s\": %S"
		    (car cell) (nth 2 cell))
	    )
	   (t
	    (format "\"%s\": { \"@type\": \"hdic:object\"%s, \"description\": %s }"
		    (car cell)
		    (if (nth 1 cell)
			(hdic-json-format-props (nth 1 cell))
		      "")
		    (hdic-json-format-list (nthcdr 2 cell) iri))
	    ))
	  )
	 ((stringp cell)
	  (format "\"name\": %S" cell)
	  )
	 (t
	  (format "\"%s\": { \"@type\": \"hdic:%s\", \"name\": %S }"
		  (car cell)(car cell)(nth 2 cell))
	  )))
      children ", "))))

(defun hdic-json-format-def (props children iri-base number)
  (let ((iri (format "%s#s%d" iri-base number))
	cobj entry-char syp-id ; ret
	)
    (format
     "{ \"@type\": \"Def\", \"@id\": \"%s\", \"type\": \"synonym\", %s }"
     iri
     (mapconcat
      (lambda (cell)
	(cond
	 ((consp cell)
	  (cond
	   ((memq (car cell) '(prefix exact suffix))
	    (format "\"%s\": %S"
		    (car cell) (nth 2 cell))
	    )
	   ((eq (car cell) 'target)
	    (setq cobj (concord-kanbun-add-morpheme-entry (nth 2 cell)))
	    (setq syp-id (file-name-nondirectory iri-base))
	    (if (string-match "#" syp-id)
		(setq syp-id (substring syp-id 0 (match-beginning 0))))
	    (setq syp-id (intern syp-id))
	    (when (setq entry-char (hdic-syp-decode-char syp-id))
              ;; (if (setq ret (get-char-attribute entry-char '<-HDIC-SYP))
              ;;     (setq entry-char (car ret)))
	      (concord-object-adjoin cobj 'synonymous-character entry-char))
	    (format "\"target\": { \"@type\": \"chise:Entry\", \"@id\": \"%s\", \"name\": \"%s\" }"
		    (hdic-json-object-iri cobj)
		    (nth 2 cell))
	    )
	   (t
	    (format "\"%s\": { \"@type\": \"hdic:object\"%s, \"description\": %s }"
		    (car cell)
		    (if (nth 1 cell)
			(hdic-json-format-props (nth 1 cell))
		      "")
		    (hdic-json-format-list (nthcdr 2 cell) iri))
	    ))
	  )
	 ((stringp cell)
	  (format "\"name\": %S" cell)
	  )
	 (t
	  (format "\"%s\": { \"@type\": \"hdic:%s\", \"name\": %S }"
		  (car cell)(car cell)(nth 2 cell))
	  )))
      children ", "))))

(defun hdic-json-format-this (props children iri-base number)
  (let ((iri (format "%s#s%d" iri-base number)))
    (format
     "{ \"@type\": \"ListItem\", \"@id\": \"%s\", \"description\": %s }"
     iri
     (hdic-json-format-list children iri))))

(defun hdic-json-format-alt (props children iri-base number)
  (let ((iri (format "%s#s%d" iri-base number)))
    (format
     "{ \"@type\": \"Alt\", \"@id\": \"%s\", \"description\": %s }"
     iri
     (hdic-json-format-list children iri))))

(defun hdic-json-format-subj (props children iri-base number)
  (let ((iri (format "%s#s%d" iri-base number)))
    (format
     "{ \"@type\": \"SubjItem\", \"@id\": \"%s\", \"name\": \"%s\" }"
     iri
     (car children))))

(defun hdic-json-format-alt-subj (props children iri-base number)
  (let ((iri (format "%s#s%d" iri-base number))
	(object (plist-get props :object)))
    (format
     "{ \"@type\": \"AltSubjItem\", \"@id\": \"%s\"%s, \"name\": \"%s\" }"
     iri
     (if object
	 (format ", \"sameAs\": \"%s\""
		 (hdic-json-object-iri object))
       "")
     (car children))))

(defun hdic-json-format-unit (unit iri-base number)
  (let (name props children)
    (cond
     ((stringp unit)
      (format "%S" unit)
      )
     ((consp unit)
      (setq name (car unit)
	    props (nth 1 unit)
	    children (nthcdr 2 unit))
      (cond
       ((eq name 'pron)
	(hdic-json-format-pron props children iri-base number)
	)
       ((eq name 'cite)
	(hdic-json-format-cite props children iri-base number)
	)
       ((eq name 'relation)
	(hdic-json-format-relation props children iri-base number)
	)
       ((eq name 'def)
	(hdic-json-format-def props children iri-base number)
	)
       ((eq name 'this)
	(hdic-json-format-this props children iri-base number)
	)
       ((eq name 'alt)
	(hdic-json-format-alt props children iri-base number)
	)
       ((eq name 'subj)
	(hdic-json-format-subj props children iri-base number)
	)
       ((eq name 'alt-subj)
	(hdic-json-format-alt-subj props children iri-base number)
	)
       (t
	(format
	 "{ \"@type\": \"%s\", \"@id\": \"%s#s%d\"%s, \"description\": %s }"
	 (capitalize (symbol-name name))
	 iri-base number
	 (if props
	     (hdic-json-format-props props)
	   "")
	 (hdic-json-format-list children (format "%s#s%d" iri-base number))
	 )))
      ))))

(defun hdic-json-format-list (units iri-base)
  (let ((i 0))
    (format "[ %s ]"
	    (mapconcat (lambda (cell)
			 (setq i (1+ i))
			 (hdic-json-format-unit cell iri-base i))
		       units ", "))))

(defun hdic-json-format-entry (entry &optional id desc)
  (unless id
    (setq id (get-char-attribute entry '=hdic-syp-entry-id)))
  (unless desc
    (setq desc (get-char-attribute entry 'hdic-syp-description)))
  (let (iri-base name)
    (setq iri-base (format "http://hdic.jp/data/syp/%s" id))
    (format "{ \"@context\": \"http://api.chise.org/contexts/v2/hdic.jsonld\", \"@type\": \"DefinedTerm\", \"@id\": \"%s\", \"sameAs\": \"%s\", %s%s\"description\": %s }"
	    iri-base
	    (hdic-json-object-iri entry)
	    (cond
	     ((setq name (get-char-attribute entry '<-HDIC-SYP))
	      (if (consp name)
		  (setq name (car name)))
	      (setq name
		    (if (char-ucs name)
			(char-to-string name)
		      (www-uri-encode-object name)))
	      (format "\"name\": \"%s\", " name)
	      )
	     (t
	      ""))
	    (cond ((and (consp (car desc))
			(eq (car (car desc)) 'this))
		   (format "\"current\": \"%s#1\", "
			   iri-base)
		   )
		  ((and (consp (nth 1 desc))
			(eq (car (nth 1 desc)) 'this))
		   (format "\"current\": \"%s#2\", "
			   iri-base)
		   )
		  (t
		   ""))
	    (hdic-json-format-list desc iri-base))))

(defun hdic-json-dump-entry (entry)
  (let (id source desc dir buf dest-buf)
    (cond
     ((setq id (get-char-attribute entry '=hdic-syp-entry-id))
      (setq source 'syp
	    desc (get-char-attribute entry 'hdic-syp-description))
      ))
    (when source
      (setq dir (expand-file-name (symbol-name source) hdic-json-dump-directory))
      (unless (file-exists-p dir)
	(make-directory dir t))
      (with-temp-buffer
	(insert (hdic-json-format-entry entry id desc))
        ;; (shell-command-on-region (point-min)(point-max) "jq"
        ;;                          (current-buffer) t)
	(setq buf (current-buffer))
	(with-temp-buffer
	  (setq dest-buf (current-buffer))
	  (if (= (with-current-buffer buf
		   (call-process-region (point-min)(point-max)
					"jq" nil
					dest-buf)) 0)
	      (write-region (point-min)(point-max)
			    (expand-file-name (format "%s.jsonld" id) dir))
	    (write-region (point-min)(point-max)
			  (expand-file-name (format "%s.jsonld.err" id) dir))
	    (with-current-buffer buf
	      (write-region (point-min)(point-max)
			    (expand-file-name (format "%s.jsonld" id) dir)))))))))
	    

;;; @ End.
;;;

(provide 'hdic-json)

;;; hdic-json.el ends here
